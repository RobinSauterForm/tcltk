puts "Civilization IV Theme - Baba Yetu"

proc Turret {} {
	proc Dark {} { return "Dark Before Dawn" }
	puts "Turret Redemption Line"
	puts [Dark]
}

proc TheOffice {{nom "He"} {excl !}} {
	set text "That's What $nom Said $excl"
	puts $text
}

proc BestBobMarleySong { Song } {
	return "It's all good, man"
}

proc track args {
	Turret
}

proc decrI {nameV} {
	upvar $nameV i 
	set i [expr $i - 1]
}

proc incrI {nameV} {
	#global price
	#return [expr $nameV * $price]
	#upvar prend la valeur price dans le code appelant, pas forcement global
	upvar price priceUp 
	return [expr $nameV * $priceUp]
}


Turret
TheOffice
TheOffice "She"
TheOffice "She" !!!
puts [BestBobMarleySong Exodus]
puts [BestBobMarleySong "I Shot The Sheriff"]
track art tyu nht

set price 42.69
puts "Price for x : [incrI 3]"

set a 18
decrI a
puts $a