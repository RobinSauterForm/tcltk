puts "Welcome to the Jungle"

puts "We've got fun 'n' games"

set priceOne 8.5
set priceOneOver4 6.3

puts "But it's the price you pay : One person : $priceOne , 6 person : [expr $priceOneOver4 * 6]"

puts "You can taste the bright lights, But you won't get them for free"

set priceBrightLights 2.0

gets stdin person

if { $person >= 4 } {
	puts "Price : [expr $priceOneOver4 * $person + $priceBrightLights * int(ceil($person / 3))]"
} else {
	puts "Price : [expr $priceOne * $person]"
}

#swicth $person {
#	1 { }
#	default {}
#}


for { set j 1 } { $j <= 7 } { incr j } {
	puts "$j jours : [expr $priceOne * $j] e"
}

set dest {"Highway To Hell" "Stairway to heaven" "Welcome to the Jungle" "Fields of Gold" "Walking on the Moon" "Ghost Division"}

for { set i 0 } { $i < [llength $dest] } { incr i } {
	puts " Destination unknown : 
	 [lindex $dest $i] ,
	 [lindex $dest [expr ( $i + 1) % [llength $dest] ] ] ,
	 [lindex $dest [expr ( $i + 2) % [llength $dest] ] ] "
}

for { set i 0 } { $i < [llength $dest] } { incr i } {
	for { set x 0 } { $x < [llength $dest] } { incr x } {
		for { set y 0 } { $y < [llength $dest] } { incr y } {
			if { $i != $x && $i != $y && $x != $y } {
				puts "Destination unknown : [lindex $dest $i] - [lindex $dest $x] - [lindex $dest $y]"
			}
		}
	}
}

# -ascii / -integer / -real
# -sorted : si déjà ord
# -all : tous les el trouvés
# -start x : commence à l'el x
# -exact / -glob par déf / -regexp
puts "Where Mother land ? : [lsearch -nocase -ascii $dest "Ghost Division"]"

# -increasing / -decreasing
puts [set destOrder [lsort -ascii -nocase -increasing $dest]]

set destR [lindex $destOrder 4]
puts [set destRr [lindex $destR 3]]

puts "Nb word : [llength $destRr]"

puts "Nb cara : [string length $destRr]"
puts "Cara index 3 : [string index $destRr 3]"

puts "Upper : [string toupper [string trim $destR]]"

puts "Empl du dernier d : [string last d [string totitle $destR]]"

set lp ""

for { set i 0 } { $i < [llength $dest] } { incr i } {
	if { [string last e $i] != -1 } {
		lappend lp $i
	}
}
puts [lsort $lp]

foreach i $dest {
	if { [string first e $i] != -1 } { puts "- $i" }
}


#tab

set bob(esp) Asc
set bob(age) 12
set bob(loca) "Moon"

puts "Bob $bob(esp)"
puts "Bob a [array size bob]"
puts "Bob $bob(esp)"
if { [lsearch [array names bob] age] != -1 } {
	puts "Age : $bob(age)"
}
if { "loca" in [array names bob] } {
	puts "Location : $bob(loca)"
}

#dict 

set "Over the Hills and Far Away" [dict create Band "Nightwish" Album "Bestwishes" Year 2001]
dict append "Over the Hills and Far Away" Type "Metal"

if { [dict exists ${Over the Hills and Far Away} Band] } {
	puts [dict get ${Over the Hills and Far Away} Band]
}



set dest2 {"Highway To Hell" "Stairway to heaven" "Welcome to the Jungle" "Fields of Gold" "Walking on the Moon" "Ghost Division" "Highway To Hell"}

set f [dict create letter 0]

#foreach i $dest2 {
#	if { [string index [string index $i 0] 0 ] == "H" } {
#		if { [dict exists $f letter] } {
#			puts "fz"
#		}
#	}
#}

set initiales [dict create]
foreach i [lsort $dest2] {
	set init [string index $i 0]
	if { ! [dict exists $initiales $init] } {
		dict append initiales $init 1
	} else {
		dict set initiales $init [expr [dict get $initiales $init]+1]
	}
}
puts $initiales


set nbMusic [list \
				 [list 42 69 420] \
				 [list 42069 69420] \
				 [list 13 666 911]
			]

puts "nbMusic : [lindex $nbMusic 0 0]"

set metaData [ list \
	[dict create name "eee" len 3.59] \
	[dict create name "ooo" len 7.25] \
	[dict create name "aaa" len 12.01] \
]


if { [llength $metaData] > 0 && [dict exists [lindex $metaData 0 ] len] } {
	puts [dict get $[lindex $metaData 0 ] len]
}

set start [clock microseconds]
puts "Time Year : [expr [clock seconds] / 60 / 60 / 24 / 365 + 1970]"
puts "Time : [clock microseconds]"
puts "Time Tom: [clock add  [clock seconds] 1 days]"
puts "Time Tom: [clock format [clock seconds] -format {%Y-%m-%d %H:%M:%S} ]"
set end [clock microseconds]
puts "Exc : [expr $end - $start] µs"