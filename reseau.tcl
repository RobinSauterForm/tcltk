package require Thread

set threadserveur [thread::create -joinable {

	set stop 0

	proc on_message {connexion} {
		gets $connexion name
		if {$name eq "STOP"} {
			global stop
			set stop 1
		} else {
			switch $name {
				Prime {set age 120.0}
				Human {set age 80.0}
				Machine {set age 5000.0}	
				default {set age 0.0}
			}
			puts $connexion "$name : $age"
			flush $connexion 
		}
	}

	proc on_connect {connexion addresse port} {
		puts "Connexion"
		puts $connexion "Time [clock format [clock seconds] -format {%Y-%m-%d %H:%M:%S}]"
		flush $connexion
		fileevent $connexion readable "on_message $connexion"
	}

	socket -server on_connect 11000
	puts "Start"
	vwait stop
	puts "Stop"
}]

after 1000
set client [socket localhost 11000]
gets $client reponse
puts "Reponse du serveur : $reponse"

puts $client "Prime"
flush $client
gets $client reponse
puts "Reponse du serveur : $reponse"

after 5000

puts $client "STOP"
flush $client

#close $client

thread::join $threadserveur
