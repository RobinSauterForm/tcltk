package require Thread

puts "Through The Fire And Flames"

tsv::set spqrC res 0
tsv::set spqrCc tooLong 0

#puts "Spread of Rome 6 months : [SPQR_Conquest 6] m²"

set threadCalc [thread::create -joinable {

	proc SPQR_Conquest {months} {
		if {$months<3} {
			return $months
		} elseif { [tsv::get spqrCc tooLong] }  { 
			return Inf
		} else {
			return [expr 3 * ([SPQR_Conquest [expr $months-1]]+[SPQR_Conquest [expr $months-2]]) / 2]
		}
	}

	tsv::lock spqrC {
		tsv::set spqrC res [SPQR_Conquest 32]
	}
}]
puts "Spread of Rome 32 months : "
for {set i 0} {[thread::exists $threadCalc]} {incr i} { 
	puts -nonewline "."
	flush stdout
	after 500
	if {$i eq 20} { tsv::set spqrCc tooLong 1 }
}
#thread::join $threadCalc // sync to main
tsv::lock spqrC {
	puts "[tsv::get spqrC res]"
}
