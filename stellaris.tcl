
#pkg_mkIndex . stellaris.tcl
package provide Stellaris 1.0

namespace eval Stellaris {
	variable species [list Human Mammalian Reptilian Avian Arthropoid Molluscoid Fungoid Machine]
	namespace export displaySpecies
}

proc Stellaris::addSpecies {nameSpec} {
	variable species 
	lappend species $nameSpec
}

proc Stellaris::displaySpecies {} {
	variable species
	puts "Choose one specie : "
	foreach specie $species {
		puts "- $specie"
	}
}

#Stellaris::displaySpecies