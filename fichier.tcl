
if { [catch { [expr 3/0] } err ] } {
	puts "Error : $err"
}

if { [catch { error "Fost Zol" } err ] } {
	puts "Error : $err"
}

if { [catch { error "Fost Zol" "Pepsi" 10 } err ] } {
	puts "Error : $err ! $errorInfo ($errorCode)"
}


if 	{ [catch { 
	if { ! [ file exists data] } {
		file mkdir data
	}
	cd data
	puts "pwd : [pwd]"

	foreach fichier [glob -nocomplain *.*] {
		puts "- $fichier"
	}

	set fileName "log[clock format [clock seconds] -format {%y%m%d}].txt"

	puts $fileName

	set f1 [open $fileName a] 

	puts $f1 "[clock format [clock seconds] -format {%Y-%m-%d %H:%M:%S}] Ok"

	close $f1

	} err ] } {
	puts "Error : $err ! $errorInfo ($errorCode)"
}