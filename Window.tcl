package require Tk


puts "Fear Of The Dark | Paschendale | Isle of Avalon"

# window root : .
# panel 1 	  : .panel1
# label 1     : .panel1.label1	

proc test { } {
	tk::toplevel .window2
	tk::toplevel .window3
	wm title .window2 "Ghost Of The Navigator"
	wm geometry .window2 800x500+100+0
	wm attribut .window2 -alpha 0.5 -topmost 1
}

wm title . "No More Lies"
wm geometry . 600x600

tk::frame .panel1 -width 580 -height 280 -relief sunken -bd 2
place .panel1 -x 10 -y 10
##
tk::label .panel1.label1 -text "1.0_1-hellofriend.wav"
##place .panel1.label1 -x 10 -y 10 
grid .panel1.label1 -column 0 -row 0
##
tk::entry .panel1.zoneT1 -textvariable titleVar
##place .panel1.zoneT1 -x 10 -y 32 
grid .panel1.zoneT1 -column 1 -row 0

tk::label .panel1.label2 -text "1.0_6-leavem3here.flac"
##place .panel1.label2 -x 10 -y 64 
grid .panel1.label2 -column 0 -row 1
##
tk::entry .panel1.zoneT2 
##place .panel1.zoneT2 -x 10 -y 96 
grid .panel1.zoneT2 -column 1 -row 1

tk::label .panel1.label3 -text "1.1_5-illusionofchoice.mp3" 
##place .panel1.label3 -x 10 -y 128 
grid .panel1.label3 -column 0 -row 2
##
tk::text .panel1.zoneT3
##place .panel1.zoneT3 -x 10 -y 160 -width 500 -height 60
#grid .panel1.zoneT3 -column 0 -row 3 -columnspan 2

tk::checkbutton .panel1.chb -text "1.3_4-allsafevirus.bwf" 
##place .panel1.chb -x 300 -y 32
grid .panel1.chb -column 0 -row 4

tk::button .panel1.button1 -text "1.4_6-n0execution.au" -command on_pBt1
##place .panel1.button1 -x 10 -y 224
grid .panel1.button1 -column 0 -row 5






tk::labelframe .panel2 -width 580 -height 280 -relief raised -bd 2 -text "Nightcall"
place .panel2 -x 10 -y 310

tk::label .panel2.label20 -text "1.0_1-hellofriend.wav" -textvariable message
place .panel2.label20 -x 10 -y 10 

proc on_pBt1 {} {
	global message titleVar
	global .panel1.zoneT3
	set infos [.panel1.zoneT3 get 1.0 end]
	set message "Nothing Else Matters : $titleVar $infos"
}

#Quit
bind . <Control-KeyPress-q> "exit"
