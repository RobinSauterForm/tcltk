puts "A Place Where You Belong"

namespace eval EU4 {

variable adm 200.0
variable dip 100.0
variable mil 500.0

namespace export displayGov

}

proc EU4::getGov {} {
	variable adm 
	variable dip
	variable mil
	return [expr $adm + $dip + $mil]
}

proc EU4::displayGov {} {
	puts "Nb Gov points : [getGov]"
}


EU4::displayGov

namespace import EU4::*
displayGov