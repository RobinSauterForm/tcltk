
oo::class create StellarisSpecie {
	variable name
	variable type
	variable maxAge

	constructor {{n "Default"} {t "Default Specie"} {m 100.0}} { 
		set name $n 
		set type $t
		set maxAge $m
	}

	destructor { puts "$name delete" }

	method setName {n} { set name $n }
	method setType {t} { set type $t }
	method setMaxAge {m} { set maxAge $m }
	method setMaxAgeMonth {m} { my setMaxAge $m / 12 }

	method display {} {
		puts -nonewline "Specie : $name , $type , $maxAge"
	}

}

oo::class create Encadreur {
	method encadrer {} {
		puts "******* -- ****** --- ***** - *"
		my display
		puts "*** --- ** - **** -- ** --- ******* --"
	}
}

oo::class create StellarisSubSpecie {
	superclass StellarisSpecie
	mixin Encadreur

	variable subType

	constructor {{n "Default"} {t "Default Specie"} {m 100.0} {st "Default SubSpecie"}} { 
		next $n $t $m
		set subType $st
	}

	method setSubType {st} { set subType $st }

	method display {} {
		puts "[next] , $subType"
	}

}


set prime [StellarisSpecie new]
$prime display
$prime setName "Prime"
$prime setType "Prime Human"
$prime setMaxAge 120.0
$prime display
$prime destroy



set android [StellarisSubSpecie new "Android-1" "Machine" 5000.0 "Android"]
$android display